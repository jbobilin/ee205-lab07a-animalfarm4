///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalfactory.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   6 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include "animal.hpp"
#include "fish.hpp"
#include "bird.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "aku.hpp"
#include "nunu.hpp"
#include "palila.hpp"
#include "nene.hpp"


#pragma once

using namespace std;

namespace animalfarm {

//animalfactory class declaration
class AnimalFactory {
public:
   static Animal* getRandomAnimal();
};
} // namespace animalfarm
