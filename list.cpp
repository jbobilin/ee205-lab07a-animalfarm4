///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Implementation file for linked list functions
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   26 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "list.hpp"

using namespace std;

namespace animalfarm {

// return true if list is empty, false if anything in list
const bool SingleLinkedList::empty() const { 
   if (head == NULL)
      return true;
   else
      return false;
}


// add newNode to front of list
void SingleLinkedList::push_front( Node* newNode ){
  Node *temp = newNode;
  temp -> next = head;
  head = temp;
}


//remove a node from the front of the list, if list is already empty return nullptr
Node* SingleLinkedList::pop_front() {
   if (head == NULL){
      return nullptr;
   }
   else {
      Node *temp = head;
      head = head->next;
      return temp;
   }
}


//return the very first node from the list
Node* SingleLinkedList::get_first() const {
   return head;
}


//return the node immediately following currentNode
Node* SingleLinkedList::get_next( const Node* currentNode ) const {
   return currentNode -> next;

}


//return number of nodes in the list
unsigned int SingleLinkedList::size() const{
   int count = 0;
   Node* current = head;
   while (current != NULL) {
      current = current -> next;
      count ++;
   }
   return count;
}


} // namespace animalfarm
