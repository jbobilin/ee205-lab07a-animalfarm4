///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   26 March 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "time.h"
#include "node.hpp"
#include "list.hpp"
#include "animal.hpp"
#include "animalfactory.hpp"
#include "fish.hpp"
#include "bird.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "aku.hpp"
#include "nunu.hpp"
#include "palila.hpp"
#include "nene.hpp"

//namespaces
using namespace std;
using namespace animalfarm;

int main() {
   //print title
   cout << "Welcome to Animal Farm 4" << endl;

   ///////////////////////////////                ///////////////////////////////
   ///////////////////////////////  Animal List   ///////////////////////////////
   ///////////////////////////////                ///////////////////////////////

   //seed rand
   srand(time(NULL));

   //instantiate a SingleLinkedList
   SingleLinkedList animalList;

   //create list of animals
   for( auto i = 0 ; i < 25 ; i++ ){
      animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
   }

   //print info
   cout << endl;
   cout << "List of Animals" << endl;
   cout << "  Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "  Number of elements: " << animalList.size() << endl;

   //iterate over list
   for( auto animal = animalList.get_first()        // for() initialize
      ; animal != nullptr                           // for() test
      ; animal = animalList.get_next( animal )) {   // for() increment
      cout << ((Animal*)animal)->speak() << endl;
   }

   //delete list / deallocate memory
   while( !animalList.empty() ) {
      Animal* animal = (Animal*) animalList.pop_front();

      delete animal;
   }

   cout << endl;

   return 0;
}
