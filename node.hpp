///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// Handle node file to use in list file
///
/// @author Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   26 March 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <iostream>

using namespace std;

namespace animalfarm {

class Node {
   protected:
   Node* next = nullptr;
   friend class SingleLinkedList;
};

} // namespace animalfarm
